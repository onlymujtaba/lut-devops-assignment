
FROM node:18

WORKDIR /app

COPY package*.json ./

# fixing permission because npm v9 breaking on some cloud platform
RUN npm ci && find /app/node_modules/ ! -user root | xargs chown root:root

COPY . /app

EXPOSE 3000

CMD ["npm","run","start"]
